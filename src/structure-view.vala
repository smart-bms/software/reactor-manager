[GtkTemplate (ui="/rm/structure-view.ui")]
public class Rm.StructureView : Adw.Bin {
    public RCom.Structure structure { get; set; }
    public string identifier { get; set; }
    public string title { get; set; }
    public new bool show { get; set; }

    public signal void load_action (RCom.Structure structure);
    public signal void send_action (RCom.Structure structure);

    [GtkChild] unowned Gtk.ListBox list_box;

    Gtk.SizeGroup label_group = new Gtk.SizeGroup (Gtk.SizeGroupMode.HORIZONTAL);
    Gtk.SizeGroup value_group = new Gtk.SizeGroup (Gtk.SizeGroupMode.HORIZONTAL);
    Gtk.SizeGroup unit_group = new Gtk.SizeGroup (Gtk.SizeGroupMode.HORIZONTAL);

    construct {
	notify["structure"].connect (bind_structure);
    }

    ~StructureView () {
	message ("~StructureView");
    }

    void bind_structure () {
	if (structure != null) {
	    var model = new StructureModel (this);

	    list_box.bind_model (model, model.build_item_widget);
	} else {
	    list_box.bind_model (null, null);
	}
    }

    [GtkCallback]
    void on_load_clicked () {
	load_action (structure);
    }

    [GtkCallback]
    void on_send_clicked () {
	send_action (structure);
    }

    internal Gtk.Widget build_item_widget (StructureEntryModel item) {
	Gtk.Widget? widget = null;

	if (item is StructureIntModel) {
	    var model = item as StructureIntModel;
	    var spin = new Gtk.SpinButton.with_range (model.min, model.max, 1);

	    model.bind_property ("value", spin.get_adjustment (), "value",
	    BindingFlags.SYNC_CREATE |
	    BindingFlags.BIDIRECTIONAL);

	    widget = new StructureEntryView (model, spin, label_group,
					     value_group, unit_group);
	} else if (item is StructureCurveModel) {
	    var model = item as StructureCurveModel;
	    var view = new CurveView ();

	    model.bind_property ("curve", view, "curve",
				 BindingFlags.SYNC_CREATE |
				 BindingFlags.BIDIRECTIONAL);

	    widget = new StructureEntryView (model, view, label_group,
					     value_group, unit_group);
	} else {
	    widget = new Gtk.Label ("unknown widget");
	}

	var row = new Gtk.ListBoxRow ();

	row.activatable = false;
	row.child = widget;

	return row;
    }
}

class Rm.StructureModel : ListModel, Object {
    unowned StructureView view;
    RCom.Structure structure;
    StructureEntryModel[] array = {};

    public StructureModel (StructureView view) {
	this.view = view;
	structure = view.structure;

	foreach (var prop in structure.get_class ().list_properties ()) {
	    if (RCom.Structure.is_reserved (prop.name)) {
		continue;
	    }

	    if (prop.value_type.is_a (typeof (uint))) {
		make_uint (structure, prop as ParamSpecUInt);
	    } else if (prop.value_type.is_a (typeof (int))) {
		make_int (structure, prop as ParamSpecInt);
	    } else if (prop.value_type.is_a (typeof (BCom.Curve))) {
		make_curve (structure, prop as ParamSpecObject);
	    } else if (prop.value_type.is_a (typeof (RCom.Structure))) {
		warning ("nested structures are not supported");
	    } else {
		warning ("skip property %s %s",
			 prop.value_type.name (), prop.name);
	    }
	}
    }

    public Gtk.Widget build_item_widget (Object item) {
	return view.build_item_widget (item as StructureEntryModel);
    }

    void make_uint (RCom.Structure structure, ParamSpecUInt spec) {
	array += new StructureIntModel (structure, spec,
					spec.default_value,
					spec.minimum, spec.maximum);
    }

    void make_int (RCom.Structure structure, ParamSpecInt spec) {
	array += new StructureIntModel (structure, spec,
					spec.default_value,
					spec.minimum, spec.maximum);
    }

    void make_curve (RCom.Structure structure, ParamSpecObject spec) {
	array += new StructureCurveModel (structure, spec);
    }

    Object? get_item (uint index) {
	return array[index];
    }
    
    uint get_n_items () {
	return array.length;
    }

    Type get_item_type () {
	return typeof (StructureEntryModel);
    }
}

class Rm.StructureEntryModel : Object {
    public RCom.Structure structure { get; private set; }
    public ParamSpec spec { get; private set; }
    public string unit { get; private set; }
    public double scale { get; private set; }

    protected StructureEntryModel (RCom.Structure structure, ParamSpec spec,
				   string? unit,
				   double? scale) {
	this.structure = structure;
	this.spec = spec;
	this.unit = get_attribute ("unit", unit);

	var scale_attr = get_attribute ("scale", null);

	if (scale_attr != null) {
	    this.scale = double.parse (scale_attr);
	} else {
	    this.scale = scale != null ? scale : 1;
	}
    }

    string? get_attribute (string key, string? def) {
	unowned var blurb = spec.get_blurb ();

	var needle = "[%s=".printf (key);
	var begin = blurb.index_of (needle);

	if (begin < 0) {
	    return def;
	}

	var end = blurb.index_of_char (']', begin + needle.length);

	if (end < 0) {
	    return def;
	}

	return blurb.slice (begin + needle.length, end);
    }
}

class Rm.StructureEntryView : Gtk.Box {
    public StructureEntryView (StructureEntryModel model,
			       Gtk.Widget value_widget,
			       Gtk.SizeGroup label_group,
			       Gtk.SizeGroup value_group,
			       Gtk.SizeGroup unit_group) {
	orientation = Gtk.Orientation.HORIZONTAL;
	spacing = 6;
	hexpand = true;

	var label = new Gtk.Label (model.spec.name);
	var unit = new Gtk.Label (model.unit);

	label.xalign = 1;
	label.halign = Gtk.Align.START;

	value_widget.halign = Gtk.Align.FILL;
	value_widget.hexpand = true;

	unit.xalign = 0;

	append (label);
	append (value_widget);
	append (unit);

	label_group.add_widget (label);
	value_group.add_widget (value_widget);
	unit_group.add_widget (unit);
    }

    protected override void constructed () {
	base.constructed ();
    }
}

class Rm.StructureIntModel : StructureEntryModel {
    public int64 value { get; set; }
    public int64 min { get; private set; }
    public int64 max { get; private set; }

    public StructureIntModel (RCom.Structure structure, ParamSpec spec,
			      int64 value, int64 min, int64 max) {
	base (structure, spec, "?", 1);

	this.value = value;
	this.min = min;
	this.max = max;

	structure.notify[spec.name].connect (() => {
	    var prop = Value (typeof (int));
	    prop.unset ();
	    structure.get_property (spec.name, ref prop);

	    if (prop.holds (typeof (uint))) {
		message ("updated %s %u", spec.name, prop.get_uint ());
	    }

	    if (prop.holds (typeof (int))) {
		message ("updated %s %d", spec.name, prop.get_int ());
	    }
	});

	structure.bind_property (spec.name, this, "value",
				 BindingFlags.SYNC_CREATE |
				 BindingFlags.BIDIRECTIONAL);
    }
}

class Rm.StructureCurveModel : StructureEntryModel {
    public BCom.Curve curve { get; set; }

    public StructureCurveModel (RCom.Structure structure, ParamSpec spec) {
	base (structure, spec, null, null);

	bind_property ("curve", structure, spec.name,
		       BindingFlags.SYNC_CREATE |
		       BindingFlags.BIDIRECTIONAL);
    }
}
