public class Rm.BatteryActivity : Object {
    public ConsoleModel console { get; set; }
    public BatteryModel model {
	get {
	    return p_model;
	}
	set {
	    if (value != p_model) {
		p_model = value;
	    }
	}
    }
    public BCom.Port com {
	get {
	    return p_com;
	}
	set {
	    if (value != p_com) {
		if (com != null) {
		    unbind_com ();
		}

		p_com = value;

		if (com != null) {
		    bind_com ();
		}
	    }
	}
    }
    public bool run {
	get {
	    return p_run;
	}
	set {
	    if (value != p_run) {
		p_run = value;
		update_state ();
	    }
	}
    }
    public uint timeout {
	get {
	    return p_timeout;
	}
	set {
	    if (value != p_timeout) {
		p_timeout = value;
		update_state ();
	    }
	}
    }
    public bool bat_conf_loaded { get; private set; }
    public bool prot_conf_loaded { get; private set; }

    public signal void com_error (string message);

    BatteryModel p_model;
    BCom.Port p_com;
    bool p_run;
    uint p_timeout;
    Timer timer;

    construct {
	timeout = 250;
    }

    ~BatteryActivity () {
	message ("~BatteryActivity");
	com = null;
	run = false;
    }

    public void load_structure (RCom.Structure structure) {
	message ("load %s".printf (structure.get_class ().get_name ()));
    }

    public void send_structure (RCom.Structure structure) {
	message ("send %s".printf (structure.get_class ().get_name ()));
    }

    void update_state () {
	timer = null;

	if (run) {
	    timer = new Timer (timeout, this.loop);
	}
    }

    void loop () {
	try {
	    if (com != null) {
		uint32 query = 0;

		query |= BCom.Query.VOLTAGE | BCom.Query.CURRENT;

		if (!bat_conf_loaded) {
		    query |= BCom.Query.BAT_CONF;
		}

		if (!prot_conf_loaded) {
		    query |= BCom.Query.PROT_CONF;
		}

		if (query != 0) {
		    com.send_query (query);
		}
	    }
	} catch (Error e) {
	    com_error (e.message);
	}
    }

    void unbind_com () {
	com.receive_message.disconnect (this.on_receive_message);
	com.receive_ping.disconnect (this.on_receive_ping);
	com.receive_voltage.disconnect (this.on_receive_voltage);
	com.receive_current.disconnect (this.on_receive_current);
	com.receive_bat_conf.disconnect (this.on_receive_bat_conf);
	com.receive_prot_conf.disconnect (this.on_receive_prot_conf);
    }

    void bind_com () {
	com.receive_message.connect (this.on_receive_message);
	com.receive_ping.connect (this.on_receive_ping);
	com.receive_voltage.connect (this.on_receive_voltage);
	com.receive_current.connect (this.on_receive_current);
	com.receive_bat_conf.connect (this.on_receive_bat_conf);
	com.receive_prot_conf.connect (this.on_receive_prot_conf);
    }

    void on_receive_message (string message) {
	console.log (ConsoleLog.IN, message);
    }

    void on_receive_ping (uint ack) {
	console.log (ConsoleLog.IN, "pong %u".printf (ack));

	if (ack > 0) {
	    try {
		com.send_ping (ack - 1);
		console.log (ConsoleLog.OUT, "ping %u".printf (ack - 1));
	    } catch (Error e) {
		warning ("%s", e.message);
	    }
	}
    }

    void on_receive_voltage (int16[] voltages) {
	if (model != null) {
	    model.num_cells = voltages.length;

	    uint32 sum = 0;

	    for (var i = 0; i < voltages.length; i++) {
		model.get_cell (i).voltage = voltages[i];
		sum += voltages[i];
	    }

	    model.voltage = sum;
	}
    }

    void on_receive_current (int current) {
	return_val_if_fail (model != null, -1);

	model.current = current;
    }

    void on_receive_bat_conf (BCom.BatConf config) {
	return_val_if_fail (model != null, -1);

	message ("receive bat conf %u", config.nominal_voltage);

	model.bat_conf = config;
	bat_conf_loaded = true;
    }

    void on_receive_prot_conf (BCom.ProtConf config) {
	return_val_if_fail (model != null, -1);

	message ("receive prot conf");
	model.prot_conf = config;
	prot_conf_loaded = true;
    }
}
