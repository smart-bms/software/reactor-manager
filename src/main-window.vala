[GtkTemplate (ui="/rm/main-window.ui")]
public class Rm.MainWindow : Adw.ApplicationWindow {
    public BatteryModel battery_model { get; set; }
    public RCom.Port com { get; set; }
    public BatteryActivity battery_activity { get; set; }

    [GtkChild] unowned Adw.Leaflet main_leaflet;
    [GtkChild] unowned ComView com_view;
    [GtkChild] unowned ConsoleView console_view;
    [GtkChild] unowned ConfigView config_view;

    Settings settings;

    construct {
	battery_activity = new BatteryActivity ();
	battery_activity.console = console_view.model;
	battery_activity.run = true;

	bind_property ("battery-model", battery_activity, "model",
		       BindingFlags.SYNC_CREATE);
	bind_property ("com", battery_activity, "com",
		       BindingFlags.SYNC_CREATE);

	config_view.load_action.connect (battery_activity.load_structure);
    }

    public MainWindow (Application app) {
	Object (application: app);
    }

    ~MainWindow () {
	message ("~MainWindow");
    }

    public void load_settings (Settings settings) {
	this.settings = settings;

	settings.bind ("window-page", main_leaflet, "visible-child-name",
		       SettingsBindFlags.DEFAULT);

	default_width = settings.get_int ("window-width");
	default_height = settings.get_int ("window-height");

	close_request.connect (this.on_close_request);

	com_view.load_settings (settings);
	console_view.load_settings (settings);
	config_view.load_settings (settings);
    }

    bool on_close_request () {
	settings.set_int ("window-width", get_width ());
	settings.set_int ("window-height", get_height ());
	return false;
    }
}
