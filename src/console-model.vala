public enum Rm.ConsoleLog {
    IN, OUT, ERROR, INFO,
}

public class Rm.ConsoleModel : Object {
    public RCom.Port com { get; set; }

    public signal void print (string msg);

    public void execute (string command) {
	log (INFO, "execute %s".printf (command));
    }

    public void log (ConsoleLog level, string text) {
	switch (level) {
	    case IN:
		print (format (">> ", text));
		break;

	    case OUT:
		print (format ("<< ", text));
		break;

	    case ERROR:
		print (format ("!! ", text));
		break;

	    case INFO:
		print (format (":: ", text));
		break;
	}
    }

    private string format (string prefix, string text) {
	var builder = new StringBuilder ();
	var lines = text.split ("\n");
	var tab = false;

	builder.append (prefix);

	foreach (unowned var line in lines) {
	    if (tab) {
		builder.append_c ('\n');

		for (var i = 0; i < prefix.length; i++) {
		    builder.append_c (' ');
		}
	    }

	    builder.append (line);
	    tab = true;
	}

	return (owned) builder.str;
    }
}
