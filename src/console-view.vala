[GtkTemplate (ui="/rm/console-view.ui")]
public class Rm.ConsoleView : Adw.Bin {
    public ConsoleModel model { get; private set; }
    public bool auto_scroll { get; set; }
    public uint max_lines { get; set; }

    [GtkChild] private unowned Gtk.TextView text_view;
    [GtkChild] private unowned Gtk.Entry command_entry;
    [GtkChild] private unowned Gtk.CheckButton auto_scroll_button;
    [GtkChild] private unowned Gtk.Adjustment max_lines_adjust;

    private Settings settings;

    construct {
	model = new ConsoleModel ();
	model.print.connect (print);

	notify["max-lines"].connect (trunc_max_lines);

	bind_property ("auto-scroll", auto_scroll_button, "active",
		       BindingFlags.SYNC_CREATE |
		       BindingFlags.BIDIRECTIONAL);
	bind_property ("max-lines", max_lines_adjust, "value",
		       BindingFlags.SYNC_CREATE |
		       BindingFlags.BIDIRECTIONAL);
    }

    ~ConsoleView () {
	message ("~ConsoleView");
    }

    public void print (string text) {
	var buffer = text_view.get_buffer ();
	Gtk.TextIter begin, end;

	buffer.get_start_iter (out begin);
	buffer.get_end_iter (out end);

	if (begin.compare (end) != 0) {
	    buffer.insert (ref end, "\n", -1);
	}

	buffer.insert (ref end, text, -1);

	trunc_max_lines ();

	if (auto_scroll) {
	    scroll_down ();
	}
    }

    public void load_settings (Settings settings) {
	this.settings = settings;

	settings.bind ("console-auto-scroll", this, "auto-scroll",
		       SettingsBindFlags.DEFAULT);
	settings.bind ("console-max-lines", this, "max-lines",
		       SettingsBindFlags.DEFAULT);
    }

    public void trunc_max_lines () {
	var buffer = text_view.get_buffer ();
	var count = buffer.get_line_count ();

	if (count > max_lines) {
	    Gtk.TextIter begin, end;

	    buffer.get_iter_at_line (out begin, 0);
	    buffer.get_iter_at_line (out end, (int) (count - max_lines));
	    buffer.delete (ref begin, ref end);
	}
    }

    [GtkCallback]
    private void clear_entry () {
	var buffer = text_view.get_buffer ();
	Gtk.TextIter begin, end;

	buffer.get_iter_at_line (out begin, 0);
	buffer.get_end_iter (out end);
	buffer.delete (ref begin, ref end);
    }

    [GtkCallback]
    private void scroll_down () {
	var buffer = text_view.get_buffer ();
	Gtk.TextIter end;

	buffer.get_end_iter (out end);

	var mark = buffer.create_mark (null, end, false);

	text_view.scroll_to_mark (mark, 0, true, 0, 1);
    }

    [GtkCallback]
    private void on_command_activate () {
	var text = command_entry.buffer.text;

	command_entry.buffer.set_text ((uint8[]) "");

	model.execute (text);
    }
}
