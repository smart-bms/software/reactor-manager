[GtkTemplate (ui="/rm/com-view.ui")]
public class Rm.ComView : Adw.Bin {
    public RCom.Port com { get; private set; }
    public ComViewSerialModel? serial_model { get; private set; }

    [GtkChild] private unowned Gtk.Stack page_stack;
    [GtkChild] private unowned Gtk.ListBox serial_list_box;
    [GtkChild] private unowned Gtk.Stack serial_stack;

    private Binding target_binding;
    private RCom.Serial serial;

    construct {
	com = new BCom.Port ();
	serial = new RCom.Serial ();

	serial.bind_port (com);

	serial_model = new ComViewSerialModel (this);
	serial_model.notify["is-connected"].connect (update_serial_stack);
	serial_model.notify["is-empty"].connect (update_serial_stack);

	serial_list_box.row_activated.connect (serial_model.on_row_activated);
	serial_list_box.bind_model (serial_model, create_serial_widget);

	update_serial_stack ();
	bind_current_page ();
    }

    ~ComView () {
	message ("~ComView");
    }

    public void load_settings (Settings settings) {
	settings.bind ("com-page", page_stack, "visible-child-name",
		       SettingsBindFlags.DEFAULT);

	serial_model.load_settings (settings);
    }

    private static Gtk.Widget create_serial_widget (Object object) {
	var item = object as ComViewSerialItem;
	var widget = new Gtk.Label (item.path);

	widget.margin_top = 6;
	widget.margin_bottom = 6;
	widget.margin_start = 6;
	widget.margin_end = 6;

	return widget;
    }

    private void update_serial_stack () {
	if (serial_model.is_connected) {
	    serial_stack.visible_child_name = "connected";
	} else if (serial_model.is_empty) {
	    serial_stack.visible_child_name = "empty";
	} else {
	    serial_stack.visible_child_name = "available";
	}
    }

    [GtkCallback]
    private new void disconnect () {
	serial_model.on_row_activated (null);
    }

    [GtkCallback]
    private void bind_current_page () {
	var name = page_stack.visible_child_name;

	serial_model.is_active = name == "serial";

	if (name == "serial") {
	    target_binding = serial_model
		.bind_property ("target", com, "target",
				BindingFlags.SYNC_CREATE);
	} else {
	    target_binding = null;
	    com = null;
	}
    }
}

public class Rm.ComViewSerialModel : ListModel, Object {
    public bool is_active { get; set; }
    public bool is_empty { get; set; }
    public bool is_connected { get; set; }
    public string device { get; set; default = ""; }
    public RCom.Termios target { get; set; }

    private unowned ComView view;
    private GenericArray<ComViewSerialItem> array;
    private FileMonitor monitor;
    private ComViewSerialItem? item_selected;

    public ComViewSerialModel (ComView view) {
	this.view = view;

	array = new GenericArray<ComViewSerialItem> ();

	try {
	    monitor = File
		.new_for_path ("/dev")
		.monitor_directory (FileMonitorFlags.NONE);
	    monitor.changed.connect (this.on_dev_dir_changed);

	    var dir = Dir.open ("/dev");
	    string? filename;

	    while ((filename = dir.read_name ()) != null) {
		add_file (File.new_build_filename ("/dev", filename));
	    }
	} catch (Error e) {
	    error ("%s", e.message);
	}

	notify["is-active"].connect (this.update_com);
	notify["device"].connect (this.update_com);
    }

    ~ComViewSerialModel () {
	message ("~ComViewSerialModel");
    }

    public void load_settings (Settings settings) {
	settings.bind ("com-serial-device", this, "device",
		       SettingsBindFlags.DEFAULT);
    }

    public void on_row_activated (Gtk.ListBoxRow? row) {
	message ("row activated %p", row);

	if (row != null) {
	    item_selected = get_item (row.get_index ()) as ComViewSerialItem;
	    assert (item_selected != null);
	    assert (item_selected.path != null);

	    device = item_selected.path;
	} else {
	    message ("on_row_activated null");
	    device = "";
	}
    }

    private void update_com () {
	if (!has_path (device)) {
	    device = "";
	}

	if (is_active && device != "") {
	    if (target != null) {
		target.device = device;
	    } else {
		target = new RCom.Termios (device);
		target.io_error.connect (on_io_error);
	    }

	    try {
		target.connect ();
	    } catch (Error e) {
		warning ("%s", e.message);
	    }
	    is_connected = true;
	} else {
	    target = null;
	    is_connected = false;
	}
    }

    private void on_io_error (Error error) {
	message ("io error: %s", error.message);
	device = "";
    }

    private void on_dev_dir_changed (File file, File? other_file,
				     FileMonitorEvent event) {
	switch (event) {
	    case FileMonitorEvent.CREATED:
		add_file (file);
		break;

	    case FileMonitorEvent.DELETED:
		remove_file (file);
		break;

	    default:
		break;
	}
    }

    private void add_file (File file) {
	var path = file.get_path ();

	if (!has_path (path) && validate_tty_file (file)) {
	    array.add (new ComViewSerialItem (file));
	    items_changed (array.length - 1, 0, 1);
	}

	is_empty = array.length < 1;
    }

    private bool has_path (string path) {
	return array.find_custom (path, compare_item_path, null);
    }

    private void remove_file (File file) {
	var path = file.get_path ();
	uint index;

	if (path == device) {
	    device = "";
	}

	if (array.find_custom (path, compare_item_path, out index)) {
	    array.remove_index (index);
	    items_changed (index, 1, 0);
	}

	is_empty = array.length < 1;
    }

    private static bool compare_item_path (ComViewSerialItem item,
					   string? path) {
	assert (path != null);
	return item.path == path;
    }

    private static bool validate_tty_file (File file) {
	return file.get_basename ().has_prefix ("ttyUSB") ||
	    file.get_basename ().has_prefix ("ttyACM");
    }

    public Object? get_item (uint index) {
	return array.get (index);
    }

    public Type get_item_type () {
	return typeof (ComViewSerialItem);
    }

    public uint get_n_items () {
	return array.length;
    }
}

private class Rm.ComViewSerialItem : Object {
    public string name { get; private set; }
    public string path { get; private set; }

    public ComViewSerialItem (File file) {
	this.name = file.get_basename ();
	this.path = file.get_path ();
    }
}
