[GtkTemplate (ui="/rm/cell-view.ui")]
public class Rm.CellView : Adw.Bin {
    public CellModel model { get; set; }
    public string index_text { get; set; }
    public string voltage_text { get; set; }

    [GtkChild] unowned Gtk.Label index_widget;
    [GtkChild] unowned Gtk.Label voltage_widget;

    public CellView (BatteryView battery, CellModel model) {
	this.model = model;

	battery.cell_label_group.add_widget (index_widget);
	battery.cell_voltage_group.add_widget (voltage_widget);

	model.bind_property ("index", this, "index-text",
			     BindingFlags.SYNC_CREATE,
			     this.format_label);
	model.bind_property ("voltage", this, "voltage-text",
			     BindingFlags.SYNC_CREATE,
			     this.format_voltage);
    }

    bool format_label (Binding binding,
			       Value index,
			       ref Value text) {
	text.set_string ("%u.".printf (index.get_uint ()));

	return true;
    }

    bool format_voltage (Binding binding,
				 Value voltage,
				 ref Value text) {
	text.set_string ("%umV".printf (voltage.get_uint ()));
	
	return true;
    }
}
