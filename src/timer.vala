public class Rm.Timer : Object {
    public delegate void Run ();

    public Timer (uint timeout, Run run) {
	source = launch (timeout, run);
    }

    private uint source;

    private static uint launch (uint timeout, Run run) {
	return Timeout.add (timeout, () => {
	    run ();
	    return Source.CONTINUE;
	});
    }

    ~Timer () {
	message ("~Timer");
	Source.remove (source);
    }
}
