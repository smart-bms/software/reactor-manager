[GtkTemplate (ui="/rm/battery-view.ui")]
public class Rm.BatteryView : Adw.Bin {
    public BatteryModel model {
	get {
	    return p_model;
	}
	set {
	    if (value != p_model) {
		if (p_model != null) {
		    foreach (var binding in bindings) {
			binding.unbind ();
		    }

		    bindings = null;
		}

		p_model = value;

		if (p_model != null) {
		    bindings = bind_model (this);
		}
	    }
	}
    }
    public Gtk.SizeGroup cell_label_group { get; set; }
    public Gtk.SizeGroup cell_voltage_group { get; set; }
    public string voltage_label { get; set; }
    public string current_label { get; set; }

    BatteryModel p_model;
    Binding[] bindings;

    [GtkChild] unowned Gtk.ListBox cell_list_box;

    construct {
	init (this);

	cell_label_group = new Gtk.SizeGroup (Gtk.SizeGroupMode.HORIZONTAL);
	cell_voltage_group = new Gtk.SizeGroup (Gtk.SizeGroupMode.HORIZONTAL);
    }

    static void init (BatteryView self) {
	void *self_p = self;

	self.cell_list_box.bind_model (new CellListModel (self), (item) => {
	    return new CellView (((BatteryView) self_p), item as CellModel);
	});
    }

    ~BatteryView () {
	model = null;
    }

    static Binding[] bind_model (BatteryView self) {
	var bindings = new Binding[0];

	bindings += self.model
	    .bind_property ("voltage", self, "voltage-label",
			    BindingFlags.SYNC_CREATE,
			    format_1000);

	bindings += self.model
	    .bind_property ("current", self, "current-label",
			    BindingFlags.SYNC_CREATE,
			    format_1000);

	return (owned) bindings;
    }

    static bool format_1000 (Binding binding,
				     Value scalar,
				     ref Value text) {
	var numeric = Value (typeof (int64));

	scalar.transform (ref numeric);
	text.set_string ("%.3lf".printf (numeric.get_int64 () / 1000.0));

	return true;
    }
}

class Rm.CellListModel : ListModel, Object {
    weak BatteryView view;
    uint recent_count;

    public CellListModel (BatteryView view) {
	this.view = view;

	view.notify["model"].connect (this.update_items);

	update_items ();
    }

    public Object? get_item (uint index) {
	return view.model.get_cell (index);
    }

    public Type get_item_type () {
	return typeof (CellModel);
    }

    public uint get_n_items () {
	return view.model != null ? view.model.num_cells : 0;
    }

    void update_items () {
	if (get_n_items () > recent_count) {
	    items_changed (recent_count, 0,
			   get_n_items () - recent_count);
	} else if (get_n_items () < recent_count) {
	    items_changed (get_n_items (),
			   recent_count - get_n_items (), 0);
	}

	recent_count = get_n_items ();
    }
}
