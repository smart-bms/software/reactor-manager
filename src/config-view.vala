[GtkTemplate (ui="/rm/config-view.ui")]
public class Rm.ConfigView : Adw.Bin {
    public BatteryModel battery { get; set; }
    public bool show_battery { get; set; }
    public bool show_protection { get; set; }

    public signal void load_action (RCom.Structure structure);
    public signal void send_action (RCom.Structure structure);

    [GtkChild] unowned StructureView battery_view;
    [GtkChild] unowned StructureView protection_view;

    ~ConfigView () {
	message ("~ConfigView");
    }

    public void load_settings (Settings settings) {
	settings.bind ("config-show-battery", this, "show-battery",
		       SettingsBindFlags.DEFAULT);
	settings.bind ("config-show-protection", this, "show-protection",
		       SettingsBindFlags.DEFAULT);

	battery_view.load_action.connect (on_load_action);
	battery_view.send_action.connect (on_send_action);

	protection_view.load_action.connect (on_load_action);
	protection_view.send_action.connect (on_send_action);
    }

    void on_load_action (RCom.Structure structure) {
	load_action (structure);
    }

    void on_send_action (RCom.Structure structure) {
	send_action (structure);
    }
}
