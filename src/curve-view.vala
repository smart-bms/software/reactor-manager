[GtkTemplate (ui="/rm/curve-view.ui")]
public class Rm.CurveView : Adw.Bin {
    public BCom.Curve curve { get; set; }
    public string x_axis_label { get; private set; }
    public string y_axis_label { get; private set; }

    public int min_x { get; set; }
    public int max_x { get; set; }
    public int min_y { get; set; }
    public int max_y { get; set; }

    public double x_min_limit { get; private set; default = int.MIN; }
    public double x_max_limit { get; private set; default = int.MAX; }
    public double y_min_limit { get; private set; default = int.MIN; }
    public double y_max_limit { get; private set; default = int.MAX; }

    [GtkChild] unowned Gtk.SizeGroup x_label_group;
    [GtkChild] unowned Gtk.SizeGroup y_label_group;
    [GtkChild] unowned Gtk.SizeGroup spin_group;
    [GtkChild] unowned Gtk.ListBox list_box;

    CurveModel model;

    construct {
	x_axis_label = "(1, 2)";
	y_axis_label = "(5, 8)";

	model = new CurveModel (this);

	list_box.bind_model (model, model.build_item_widget);
    }

    ~CurveView () {
	message ("~CurveView");
    }

    [GtkCallback]
    void create_item () {
	model.create_item ();
    }

    [GtkCallback]
    void remove_item () {
	model.remove_item ();
    }

    internal Gtk.Widget build_item_widget (CurveItem item) {
	var widget = new CurveRowView (x_label_group, y_label_group,
				       spin_group);

	bind_property ("min-y", widget.y_adjustment, "lower",
		       BindingFlags.SYNC_CREATE |
		       BindingFlags.BIDIRECTIONAL);
	bind_property ("max-y", widget.y_adjustment, "upper",
		       BindingFlags.SYNC_CREATE |
		       BindingFlags.BIDIRECTIONAL);

	item.bind_property ("x-value", widget, "x-value",
			    BindingFlags.SYNC_CREATE |
			    BindingFlags.BIDIRECTIONAL);
	item.bind_property ("y-value", widget, "y-value",
			    BindingFlags.SYNC_CREATE |
			    BindingFlags.BIDIRECTIONAL);

	return widget;
    }
}

internal class Rm.CurveModel : ListModel, Object {
    weak CurveView view;
    GenericArray<CurveItem> array = new GenericArray<CurveItem> ();

    public CurveModel (CurveView view) {
	this.view = view;

	items_changed.connect (recalculate_items_x);

	view.notify["min-x"].connect (recalculate_items_x);
	view.notify["max-x"].connect (recalculate_items_x);

	create_item ();
	create_item ();
    }

    public void create_item () {
	array.add (new CurveItem (this));

	items_changed (array.length - 1, 0, 1);
    }

    public void remove_item () {
	if (get_n_items () > 2) {
	    array.remove_index (array.length - 1);

	    items_changed (array.length, 1, 0);
	}
    }

    public Gtk.Widget build_item_widget (Object item) {
	return view.build_item_widget (item as CurveItem);
    }

    void recalculate_items_x () {
	var count = get_n_items ();
	var min_x = view.min_x;
	var max_x = view.max_x;

	if (count > 1) {
	    for (var i = 0; i < count; i++) {
		var item = get_item (i) as CurveItem;
		var xs = (double) i / (count - 1);

		xs *= max_x - min_x;
		xs += min_x;

		item.x_value = (int) xs;
	    }
	} else if (count == 1) {
	    var item = get_item (0) as CurveItem;

	    item.x_value = min_x;
	}
    }

    Object? get_item (uint index) {
	return array.get (index);
    }

    uint get_n_items () {
	return array.length;
    }

    Type get_item_type () {
	return typeof (CurveItem);
    }
}

internal class Rm.CurveItem : Object {
    public int x_value { get; set; }
    public int y_value { get; set; }

    weak CurveModel model;

    public CurveItem (CurveModel model) {
	this.model = model;
    }
}
